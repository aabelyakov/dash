# card.py
# ============================================================================
from pydal import DAL, Field
import os
from datetime import datetime, date, time, timedelta
from dash import Dash, dcc, html, Input, Output, callback, dash_table
import dash_bootstrap_components as dbc
import pandas as pd
from io import StringIO
from dash.dependencies import Input, Output, State
# ============================================================================
# Содержимое размещается справа от панели меню и приобретает отступы
content = {
    "margin-left": "12rem",
    "margin-right": "0.5rem",
    "padding": "2rem 1rem"
}
# ============================================================================
# card = html.H1(children="CARD")
# card = html.Div("Карточка", style=content)
# card = html.Div(children=["CARD"])
# card = "Привет"

card = dbc.Container(
    dbc.Alert("Привет, ребятки!", color="success"),
    # className="p-5",
    # className="card",
    # className="content",
    className="wrapper"
)
