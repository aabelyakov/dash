# График изменения цен во времени с настройкамии внешнего вида
# с помощью HTML

import dash
from dash import html
import plotly.graph_objects as go
from dash import dcc
import plotly.express as px
from dash.dependencies import Input, Output

app = dash.Dash()

df = px.data.stocks()

app.layout = html.Div(
    id='parent',
    children=[
        html.H1(
            id='H1',
            children='Изменение стиля с использованием HTML',
            style={
                'textAlign': 'center',
                'marginTop': 10,
                'marginBottom': 100}
        ),

    dcc.Dropdown(
        id='dropdown',
        options=[
            {'label': 'Google', 'value': 'GOOG'},
            {'label': 'Apple', 'value': 'AAPL'},
            {'label': 'Amazon', 'value': 'AMZN'},
        ],
        value='GOOG'
    ),

    dcc.Graph(id='bar_plot')
])


@app.callback(
    Output(
        component_id='bar_plot',
        component_property='figure'
    ),
    [Input(
        component_id='dropdown',
        component_property='value'
    )]
)
def graph_update(dropdown_value):
    print(dropdown_value)
    fig = go.Figure(
        [go.Scatter(
            x=df['date'],
            y=df['{}'.format(dropdown_value)],
            line=dict(color='firebrick', width=5))
        ]
    )

    fig.update_layout(
        title='Изменение цен во времени',
        xaxis_title='Время',
        yaxis_title='Цены'
    )
    return fig


if __name__ == '__main__':
    app.run_server()
    # app.run_server(debug=True)