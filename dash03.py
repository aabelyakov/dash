import dash
from dash import html
from dash import dash_table
import pandas as pd

app = dash.Dash(__name__)

# Создаем пример данных (ваш DataFrame)
data = {'Name': ['John', 'Jane', 'Bob'],
        'Age': [25, 30, 22],
        'City': ['New York', 'San Francisco', 'Los Angeles']}

df = pd.DataFrame(data)

# Задаем layout
app.layout = html.Div([
    html.H1("Привет, мир!"),

    # Добавляем dash_table.DataTable
    dash_table.DataTable(
        id='table',
        columns=[{'name': col, 'id': col} for col in df.columns],
        data=df.to_dict('records')
    )
])

# Запуск сервера Dash
if __name__ == "__main__":
    app.run_server(debug=True)
