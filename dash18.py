from hired1 import db
import dash
from dash import html, dash_table
import dash_daq as daq
import pandas as pd
import sqlite3

# Подключение к базе данных (предположим, что result_tree является
# результатом выполнения запроса к базе данных)
# conn = sqlite3.connect('your_database.db')
# query = 'SELECT * FROM your_table'  # Замените 'your_table' на вашу таблицу
# result_tree = pd.read_sql_query(query, conn)
result_tree = db(db.cls).select()
print(dir(result_tree))

app = dash.Dash(__name__)

# Представление данных в виде списка словарей для иерархической структуры
data = [
    {'name': f'Level 1 - {i}', 'value': f'Value - {i}', 'children': [
        {'name': f'Level 2 - {i}', 'value': f'Value - {i}'}
    ]} for i in range(5)
]
# print(data)

# Задаем layout
app.layout = html.Div([
    html.H1("Web-приложение для редактирования данных"),

    # Добавляем dash_daq.ToggleSwitch для раскрытия и сворачивания строк
    daq.ToggleSwitch(
        id='toggle',
        label='Toggle Rows',
        value=False
    ),

    # Добавляем dash_table.DataTable
    dash_table.DataTable(
        id='table',
        columns=[{'name': col, 'id': col, 'editable': True} for col in result_tree.column],
        data=data,  # Используем измененную структуру данных
        editable=True,
        hidden_columns=['children'],  # Скрываем колонку с дочерними данными
        style_table={'height': '300px', 'overflowY': 'auto'},  # Добавляем прокрутку
    )
])

# Запуск сервера Dash
if __name__ == "__main__":
    app.run_server(debug=True)
