# Экспорт данных в формате CSV

import dash
from dash import dash_table
import pandas as pd

# Sample data
data = {
    'Name': ['John', 'Alice', 'Bob', 'Mary'],
    'Age': [25, 30, 35, 28],
    'Country': ['USA', 'Canada', 'USA', 'Canada']
}

df = pd.DataFrame(data)

# Create a Dash application
app = dash.Dash(__name__)

# Define the layout
app.layout = dash_table.DataTable(
    id='table',
    columns=[{"name": i, "id": i} for i in df.columns],
    data=df.to_dict('records'),
    export_format="csv"
)

# Run the app
if __name__ == '__main__':
    app.run_server(debug=True)
