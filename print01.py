# https://dash.plotly.com/dash-ag-grid/printing

import dash_ag_grid as dag
from dash import Dash, html, dcc, Input, Output, ctx, callback
import pandas as pd

app = Dash(__name__)


df = pd.read_csv(
    "https://raw.githubusercontent.com/plotly/datasets/master/ag-grid/olympic-winners.csv"
)

df = df.head(50)

columnDefs = [
    {"headerName": "ID",  "valueGetter": {"function": "params.node.rowIndex + 1"}, "width": 70},
    {"field": "country"},
    {"field": "year"},
    {"field": "athlete"},
    {"field": "date"},
    {"field": "sport"},
    {"field": "total"},
]

app.layout = html.Div([
    html.Button("Printer Friendly Layout", id="grid-printer-layout-btn"),
    html.Button("Regular Layout", id="grid-regular-layout-btn"),
    dcc.Markdown("""
        ## Example of printer friendly layout
        ### Latin Text
        Lorem ipsum dolor sit amet, ne cum repudiare abhorreant. Atqui molestiae neglegentur ad nam, mei amet eros ea, 
        populo deleniti scaevola et pri. Pro no ubique explicari, his reque nulla consequuntur in. His soleat doctus 
        constituam te, sed at alterum repudiandae. Suas ludus electram te ius.
        """),
    dag.AgGrid(
        id="grid-print",
        columnDefs=columnDefs,
        rowData=df.to_dict("records"),
        defaultColDef={"filter": True, "maxWidth": 150},
        dashGridOptions={"animateRows": False}
    ),
    dcc.Markdown("""
        ### More Latin Text
        Lorem ipsum dolor sit amet, ne cum repudiare abhorreant. Atqui molestiae neglegentur ad nam, mei amet eros ea, 
        populo deleniti scaevola et pri. Pro no ubique explicari, his reque nulla consequuntur in. His soleat doctus 
        constituam te, sed at alterum repudiandae. Suas ludus electram te ius.
        """),
],
    style={"margin": 20},
)


@callback(
    Output("grid-print", "style"),
    Output("grid-print", "dashGridOptions"),
    Input("grid-printer-layout-btn", "n_clicks"),
    Input("grid-regular-layout-btn", "n_clicks"),
)
def toggle_layout(*_):
    if ctx.triggered_id == "grid-printer-layout-btn":
        return {"height": "", "width": ""}, {"domLayout": "print"}
    return {"height": 400, "width": "100%"}, {"domLayout": None}


if __name__ == "__main__":
    app.run_server(debug=True)
