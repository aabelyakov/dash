# Логический переключатель

from dash import Dash, html, Input, Output, callback
import dash_daq as daq

app = Dash(__name__)

app.layout = html.Div([
    daq.BooleanSwitch(id='our-boolean-switch', on=False),
    html.Div(id='boolean-switch-result')
])


@callback(
    Output('boolean-switch-result', 'children'),
    Input('our-boolean-switch', 'on')
)
def update_output(on):
    return f'Переключатель находится в состоянии {on}.'


if __name__ == '__main__':
    app.run(debug=True)