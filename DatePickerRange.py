import dash
from dash import dcc, html
from dash.dependencies import Input, Output
from datetime import date, datetime, timedelta


app = dash.Dash(__name__)

app.layout = html.Div([
    dcc.DatePickerRange(
        id='date-picker-range',
        start_date=datetime.now() - timedelta(days=7),
        end_date=datetime.now(),
        display_format='YYYY-MM-DD',
        persistence=True,
        persistence_type='session'
    ),
    html.Div(id='selected-dates-output')
])


@app.callback(
    Output('selected-dates-output', 'children'),
    Input('date-picker-range', 'start_date'),
    Input('date-picker-range', 'end_date')
)
def update_output(start_date, end_date):
    # start_date_object = datetime.strptime(start_date, '%Y-%m-%d')
    # start_date_string = start_date_object.strftime('%Y-%m-%d')
    #
    # end_date_object = datetime.strptime(end_date, '%Y-%m-%d')
    # end_date_string = end_date_object.strftime('%Y-%m-%d')

    return f'Нач. дата: {start_date}, Кон.дата: {end_date}'
#enddef


if __name__ == '__main__':
    app.run_server(debug=True)

