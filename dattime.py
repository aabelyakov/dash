from datetime import datetime, date, time
from pydal import DAL, Field

# Создаем подключение к базе данных SQLite
db = DAL("sqlite://rrr.sqlite")

db.define_table(
    'rrr',
    Field('date', "text"),
    Field('time', "text"),
)
# Если поля date(дата) и time(время) в таблице rrr имеют тип text,
# то в них можно записывать как объекты типа 'datetime.date',
# 'datetime.time' так и строки типа 'str'

dat = date(2030, 12, 13)
print("type(dat) =", type(dat))
# type(dat) = <class 'datetime.date'>

tim = time(20, 12, 13)
print("type(tim) =", type(tim))
# type(tim) = <class 'datetime.time'>

dt = datetime(2024, 1, 23, 1, 2, 3)
dat1 = dt.strftime("%Y-%m-%d")
print("type(dat1) =", type(dat1))
# type(dat1) = <class 'str'>

tim1=dt.strftime("%H:%M:%S")
print("type(tim1) =", type(tim1))
# type(tim1) = <class 'str'>


# Создание записи с полями date и time типа 'datetime.date'
# и 'datetime.time' соответственно
print(db.rrr.insert(date=dat, time=tim))
# 1 - номер вставленной записи

# Создание записи с полями date и time типа 'str'
print(db.rrr.insert(date=dat1, time=tim1))
# 2 - номер вставленной записи

rows = db(db.rrr).select()
print(rows)
# rrr.id,rrr.date,rrr.time
# 1,2030-12-13,20:12:13
# 2,2024-01-23,01:02:03

print(datetime(2024, 1, 24, 0, 1, 2))
# 2024-01-24 00:01:02

print(date(2002, 12, 4).isoformat())
# 2002-12-04
print(date(2002, 12, 4))
# 2002-12-04
print(time(12, 11, 10))
# 12:11:101 2030-12-13 20:12:13
# 2 2024-01-23 01:02:03

# Получение строки с датой и временем из объекта dt при помощи f-строк
# dt = datetime.now()
# print(f'День: {dt:%d}, Месяц: {dt:%B}, время: {dt:%H:%M}.')
# print(f"{dt:%Y}-{dt:%m}-{dt:%d}")

# Создание объектов date и time из строки с датой и временем соответственно
# oDate = date.fromisoformat("2024-11-30")
# print(oDate)
# 2024-11-30
#
# oTime = time.fromisoformat("13:04")
# print(oTime)
# 13:04:00


dt = datetime.now().date()
print(11111, dt)

db.commit()

tbl = "diary.sqlite"
a=f"{tbl}"
print(a)
URI = f'sqlite://"{a}"'
print(URI)
