# Табло для выбора цвета ColorPicker

from dash import Dash, html, Input, Output, callback
import dash_daq as daq

app = Dash(__name__)

app.layout = html.Div([
    daq.ColorPicker(
        id='our-color-picker',
        label='Color Picker',
        value=dict(hex='#119DFF')
    ),
    html.Div(id='color-picker-result')
])


@callback(
    Output('color-picker-result', 'children'),
    Input('our-color-picker', 'value')
)
def update_output(value):
    return f'Выбранный цвет - {value}.'


if __name__ == '__main__':
    app.run(debug=True)