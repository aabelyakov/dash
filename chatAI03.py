from pydal import DAL, Field
import dash
from dash import dash_table
from dash import html, dcc
from dash.dependencies import Input, Output
import pandas as pd

# Создаем подключение к базе данных SQLite
db = DAL("sqlite://storage.sqlite")

# Определяем таблицу "Школы", если она еще не существует
db.define_table(
    'sch',
    Field('name', length=100, unique=True),
    Field('location', length=100)
)

# Определяем таблицу "Классы", если она еще не существует
db.define_table(
    'cls',
    Field('sch_id', 'reference sch'),
    Field('name', length=100, unique=True)
)

# Определяем таблицу "Ученики", если она еще не существует
db.define_table(
    'std',
    Field('cls_id', 'reference cls'),
    Field('name', length=100)
)

# Добавим тестовых данных
if db(db.sch.id > 0).count() == 0:
    db.sch.insert(name="Школа №1", location="Город A")

if db(db.cls.id > 0).count() == 0:
    db.cls.insert(name="Класс A1", sch_id=1)

if db(db.std.id > 0).count() == 0:
    for class_id in range(1, 2):
        for student_number in range(1, 4):
            db.std.insert(name=f"Ученик{student_number}", cls_id=class_id)

# Применяем изменения в базе данных
db.commit()

# Создаем приложение Dash
app = dash.Dash(__name__)

# Обратный вызов для отображения "пучков листьев"
@app.callback(
    Output("output-div", "children"),
    [Input("datatable3", "data")]
)
def display_leaf_bundles(data3):
    df3 = pd.DataFrame(data3)

    # Объединим данные из таблиц Школы и Классы
    merged_data = pd.merge(df3, db(db.cls.id == df3['cls_id']).select(), on='cls_id')
    merged_data = pd.merge(merged_data, db(db.sch.id == merged_data['sch_id']).select(), on='sch_id')

    # Выведем "пучок листьев"
    bundle = {
        'school_name': merged_data['name_y'].iloc[0],  # Название школы из таблицы Школы
        'class_name': merged_data['name_x'].iloc[0],  # Название класса из таблицы Классы
        'students': merged_data[['id', 'name']].to_dict('records')  # "Пучок листьев" с учениками текущего класса
    }

    return [bundle]

# Макет приложения
app.layout = html.Div([
    html.H1("Табличный редактор"),
    html.P(),

    dash_table.DataTable(
        id='datatable1',
        columns=[
            {'name': 'Наименование школы', 'id': 'name'},
            {'name': 'Адрес школы', 'id': 'location'}
        ],
        data=db(db.sch.id > 0).select().as_list(),
        editable=True,
    ),

    html.P(),

    dash_table.DataTable(
        id='datatable2',
        columns=[
            {'name': 'Наименование класса', 'id': 'name'},
        ],
        data=db(db.cls.id > 0).select().as_list(),
        editable=True,
    ),

    html.P(),

    dash_table.DataTable(
        id='datatable3',
        columns=[
            {'name': 'Имя ученика', 'id': 'name'},
            {'name': 'ID класса', 'id': 'cls_id'}
        ],
        data=db(db.std.id > 0).select().as_list(),
        editable=True,
    ),

    html.P(),

    html.Div(id="output-div")
])

if __name__ == '__main__':
    app.run_server(debug=True)
