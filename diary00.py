# Экспорт данных из таблицы diary в CSV-файл
# Импорт данных из CSV-файла в таблицу diary

from pydal import DAL, Field
import os
import csv

# ============================================================================
# НАСТРОЙКИ ПРОГРАММЫ
# ~~~~~~~~~~~~~~~~~~~
# Имя файла базы данных
base = "diary.sqlite"

# Имя таблицы
tbl = "diary"

# Имя CSV-файла
CSV = "diary.csv"

# Расположение БД - Локально
PATH = ""

# Расположение БД - Отдалённо
# PATH = "/home/bel/Yandex.Disk/Python/Dash/Bin"

# Путь к файлу CSV
pathcsv = os.path.join(PATH, CSV)

# Подключение к базе данных
# db = DAL(URI, folder=PATH)

URI = f"sqlite://{base}"

db = DAL(
    # Строка соединения с основной БД
    URI,
    # ------------------------------------------------------------------------
    # Этот каталог должен существовать до момента создания БД
    folder=PATH
)
# ============================================================================
# Определяем структуру таблицы
db.define_table(
    'diary',
    Field('date', 'text'),
    Field('time', 'text'),
    Field('gluc', 'double'),
    Field('pres', 'text'),
    Field('short', 'double'),
    Field('long', 'double'),
    Field('med', 'text'),
    Field('act', 'text'),
    Field('dlt', 'boolean'),
)
# print(URI)
# print(db)
# print(db.tables)
# print(db.diary.gluc.type)
db.commit()
# ============================================================================
# Вывод всех записей из таблицы diary
entries = db(db.diary).select()


# print(entries)

# Для удаления таблицы diary и ее повторного создания
# db.diary.drop()

# Обрезать (truncate) таблицу diary, то есть, удалить из неё все записи и
# сбросить счетчик id в 1.
# db.diary.truncate('RESTART IDENTITY CASCADE')
# ============================================================================
def exp_csv():
    """
    Экспорт данных из таблицы diary в текстовый файл diary.csv
    """
    # 1-й вариант
    entries.export_to_csv_file(open(pathcsv, 'w'))

    # 2-й вариант
    # with open(pathcsv, "w") as csvfile:
    #     entries.export_to_csv_file(csvfile)
    # endwith

    print("Экспорт данных успешно завершён")


# enddef
# ============================================================================
def imp_csv():
    """
    Импорт данных из текстового файла diary.csv в таблицу diary
    """
    # 1-й вариант
    # db.diary.import_from_csv_file(open(pathcsv, 'r'))

    # 2-й вариант
    with open(pathcsv, "r", newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            db.diary.insert(
                date=row['diary.date'],
                time=row['diary.time'],
                gluc=float(row['diary.gluc']),
                pres=row['diary.pres'],
                short=float(row['diary.short']),
                long=float(row['diary.long']),
                med=row['diary.med'],
                act=row['diary.act'],
                dlt=bool(row['diary.dlt'])
            )
        # endfor
    # endwith
    db.commit()

    print("Импорт данных успешно завершён")


# enddef
# ============================================================================
if __name__ == '__main__':
    # exp_csv()
    imp_csv()
# endif
# ============================================================================
