import dash
# import dash_html_components as html
from dash import html
# import dash_core_components as dcc
from dash import dcc
from dash.dependencies import Input, Output
import plotly.express as px
import pandas as pd


df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/gapminderDataFiveYear.csv')
app = dash.Dash('python-school')

app.layout = html.Div([
    dcc.RadioItems(
        id='scale-button',
        options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
        value='Linear',
    ),

    dcc.Graph(id='graph'),

    dcc.Slider(
        id='year-slider',
        min=df['year'].min(),
        max=df['year'].max(),
        value=df['year'].min(),
        marks={str(year): str(year) for year in df['year'].unique()},
        step=None
    )
])

@app.callback(
    Output('graph', 'figure'),
    [Input('year-slider', 'value'),
    Input('scale-button', 'value')]
)
def update_figure(selected_year, scale_value):
    filtered_df = df[df.year == selected_year]

    if scale_value == 'Linear':
        log_x = False
    else:
        log_x = True
    #endif

    fig = px.scatter(
        filtered_df, x="gdpPercap", y="lifeExp",
        size="pop", color="continent", hover_name="country",
        log_x=log_x, size_max=55
    )
    return fig
#enddef

if __name__ == '__main__':
    app.run_server(debug=True)
#endif
