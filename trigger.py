from dash import dash, callback_context

# В вашем колбеке
@app.callback(
    [Output('datatable', 'data'),
     Output('output-save', 'children')],
    [Input('date-picker-range', 'start_date'),
     Input('date-picker-range', 'end_date')],
)
def update_table(start_date, end_date):
    # Ваш код для обновления данных в таблице
    data = [...]  # Замените на ваш код

    # Определение, какой компонент вызвал колбек
    triggered_component = callback_context.triggered[0]['prop_id'].split('.')[0]

    if triggered_component == 'save-button':
        return data, "Изменения успешно сохранены!"

    else:
        return data, None
