# Дневник диабетика
# =================
# В папке assets хранятся таблицы каскадных стилей, применяемых в приложении
# dcc - Dash Core Components

# Импортируется файл graph, в котором находятся средства рисования графиков
from graphs import *
from pydal import DAL, Field
from datetime import datetime, date, time, timedelta
from dash import Dash, dcc, html, Input, Output, callback, dash_table
import dash_bootstrap_components as dbc
import pandas as pd
# from io import StringIO
from dash.dependencies import Input, Output, State
# ============================================================================
# app = Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
app = Dash(
    __name__,
    suppress_callback_exceptions=True,
    external_stylesheets=[dbc.themes.BOOTSTRAP]
)
# ============================================================================
# Макет-прародитель приложения. В этот Div при переключении страниц будут
# помещаться:
# - sidebar - панель меню,
# - sidebar+content0 - панель меню и панель суточных карточек
# - sidebar+content1 - панель меню и панель таблицы
# - sidebar+content2 - панель меню и панель графиков
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])
# ============================================================================
# Макет панели меню
sidebar = html.Div([
    html.H3("Дневник диабетика"),

    html.Hr(),

    html.P("Доступные действия:", ),

    dbc.Nav([
        dbc.NavLink("Домой", href="/", active="exact"),
        dbc.NavLink("Карточка", href="/card", active="exact"),
        dbc.NavLink("Таблица", href="/table", active="exact"),
        dbc.NavLink("График", href="/graph", active="exact"),
    ],
        vertical=True,
        pills=True
    ),
],
    # Таблица стилей для панели меню
    style=SIDEBAR_STYLE,
)
# ============================================================================
# Макет таблицы с данными
tbl = dash_table.DataTable(
    id='datatable',
    columns=[
        {'name': 'id', 'id': 'diary.id', 'editable': False},
        {'name': 'Дата', 'id': 'diary.date'},
        {'name': 'Время', 'id': 'diary.time'},
        {'name': 'Сахар', 'id': 'diary.gluc'},
        {'name': 'Давление', 'id': 'diary.pres'},
        {'name': 'Короткий', 'id': 'diary.short'},
        {'name': 'Длинный', 'id': 'diary.long'},
        {'name': 'Лекарства', 'id': 'diary.med'},
        {'name': 'Активность', 'id': 'diary.act'},
        {'name': 'К удалению', 'id': 'diary.dlt'},
    ],

    # data=[],
    data=data,

    editable=True,

    row_deletable=True,

    # Высота таблицы и скроллинг
    style_table={'height': '420px', 'overflowY': 'auto'},

    # Перемещение по страницам таблицы высотой в 15 строк
    # page_size=15,
)
# ============================================================================
# Макет правой панели
content1 = html.Div([
    # Таблица с данными
    tbl,

    html.Br(),

    html.Button("Сохранить изменения", id="save-button", n_clicks=0),

    # Для вывода сообщения об успешном завершении сохранения
    html.Div(id='output-save'),

    html.Br(),

    html.Button("Новая", id="new-button", n_clicks=0),
    html.Button("Удалить", id="del-button", n_clicks=0),

    # Для вывода сообщения об успешном создании новой записи
    html.Div(id='output-new'),

    # Для вывода сообщения об удалении помеченных записей
    html.Div(id='del-div'),

],
    # Таблица стилей для правой панели
    style=CONTENT_STYLE,
)
# ===========================================================================
'''
# Выход - вывод сообщения о выбранных датах в поле selected-dates-output
# Вход - изменение начальной даты start_date
# Вход - изменение конечной даты end_date
@callback(
    Output('selected-dates-output', 'children'),
    [Input('date-picker-range', 'start_date'),
     Input('date-picker-range', 'end_date')],
)
def update_output(start_date, end_date):
    # Фильтрация записей в таблице diary по диапазону дат
    entries = db(
        (db.diary.date >= start_date) &
        (db.diary.date <= end_date)
    ).select()
    return f'Нач. дата: {start_date}, Кон. дата: {end_date}'
# enddef
# ============================================================================
# Выход - обновление таблицы DataTable
# Вход - изменение начальной даты start_date интервала дат
# Вход - изменение конечной даты end_date интервала дат
@callback(
    Output("datatable", "data"),
    [Input('date-picker-range', 'start_date'),
     Input('date-picker-range', 'end_date')],
)
def update_table(start_date, end_date):
    # Фильтрация записей в таблице diary по интервалу дат
    entries = db(
        (db.diary.date >= start_date) &
        (db.diary.date <= end_date)
    ).select()
    # print(entries)

    data = [{
        'diary.id': entry.id,
        'diary.date': entry.date,
        'diary.time': entry.time,
        'diary.gluc': entry.gluc,
        'diary.pres': entry.pres,
        'diary.short': entry.short,
        'diary.long': entry.long,
        'diary.med': entry.med,
        'diary.act': entry.act,
        'diary.dlt': entry.dlt
    } for entry in entries
    ]

    # print(data)
    return data
# enddef
'''
# ============================================================================
# Выход - вывод сообщения о сохранении изменений в поле output-save
# Вход - нажатие кнопки "Сохранить изменения"
# Вход - выполнение редактирования полей таблицы diary. Преобразование
# данных из DataFrame в DataTable для отображения
@callback(
    Output("output-save", "children"),
    Input("save-button", "n_clicks"),
    Input("datatable", "data")
)
def save_changes(n_clicks, data):
    if n_clicks:
        # Преобразование данных из DataTable в DataFrame
        df = pd.DataFrame(data)
        # print(df)
        # Сохранение изменений в базе данных
        for index, row in df.iterrows():
            db(db.diary.id == row['diary.id']).update(
                date=row['diary.date'],
                time=row['diary.time'],
                gluc=round(float(row['diary.gluc']), 1),
                pres=row['diary.pres'],
                short=row['diary.short'],
                long=row['diary.long'],
                med=row['diary.med'],
                act=row['diary.act'],
                dlt=row['diary.dlt']
            )
        # endfor

        db.commit()

        return html.Div("Изменения успешно сохранены!", id="output-div")
    # endif
# enddef
# ============================================================================
# Выход - создание в таблице diary новой записи
# Вход - нажатие кнопки "Новая"
@callback(
    Output("datatable", "data", allow_duplicate=True),
    Input("new-button", "n_clicks"),
    State("datatable", "data"),
    prevent_initial_call=True,
)
def create_new(n_clicks, data):
    if n_clicks:
        # Вставка новой записи с new_id в таблицу diary БД
        new_id = db.diary.insert(
            date="",
            time="",
            gluc=0,
            pres="",
            short=0,
            long=0,
            med="",
            act="",
            dlt=False
        )

        # Сброс буферов программы на диск
        db.commit()

        # Запрос к таблице diary на выборку всех записей
        entries = db(db.diary).select()

        # print(entries)

        data = [{
            'diary.id': entry.id,
            'diary.date': entry.date,
            'diary.time': entry.time,
            'diary.gluc': entry.gluc,
            'diary.pres': entry.pres,
            'diary.short': entry.short,
            'diary.long': entry.long,
            'diary.med': entry.med,
            'diary.act': entry.act,
            'diary.dlt': entry.dlt

        } for entry in entries
        ]

        # print(data)
        # print(f"New ID: {new_id}")
        return data
    # endif
# enddef
# ============================================================================
# Выход - вывод сообщения  об успешном создании новой записи
# Вход - нажатие кнопки "Новая"
@callback(
    Output("output-new", "children", allow_duplicate=True),
    Input("new-button", "n_clicks"),
    prevent_initial_call=True,
)
def out_new(n_clicks):
    if n_clicks:
        return html.Div("Новая запись успешно создана!", id="output-new")
    # endif
# enddef
# ============================================================================
# Выход - вывод сообщения  об успешном об удалении помеченных записей
# Вход - нажатие кнопки "Удалить"
# Вход - удаление помеченных записей из таблицы diary
@callback(
    # Output("del-div", "children"),
    Output("datatable", "data", allow_duplicate=True),
    Input("del-button", "n_clicks"),
    State("datatable", "data"),
    prevent_initial_call=True,
)
def del_records(n_clicks, data):
    if n_clicks:
        # Удаление помеченных записей из таблицы diary
        n = db(db.diary.dlt==True).delete()
        # print(n)
        db.commit()

        # Запрос к таблице diary на выборку всех записей
        entries = db(db.diary).select()

        data = [{
            'diary.id': entry.id,
            'diary.date': entry.date,
            'diary.time': entry.time,
            'diary.gluc': entry.gluc,
            'diary.pres': entry.pres,
            'diary.short': entry.short,
            'diary.long': entry.long,
            'diary.med': entry.med,
            'diary.act': entry.act,
            'diary.dlt': entry.dlt

        } for entry in entries
        ]

        # # Преобразование данных из DataTable в DataFrame
        # df = pd.DataFrame(data)
        # # print(df)
        #
        # return html.Div("Записи успешно удалены!", id="del-div")
        return data
    # endif
# enddef
#=============================================================================
# Обновление панели меню
# Выход - страница в браузере
# Вход - путь в поисковой строке браузера
@callback(
    Output('page-content', 'children'),
    Input('url', 'pathname')
)
def display_page(pathname):
    if pathname == '/':
        # Домой - Макет приложения с боковым меню sidebar
        return html.Div([sidebar])
    elif pathname == '/card':
        # Карточки - Макет приложения с боковым меню sidebar и правой панелью
        # content0
        return html.Div([sidebar, content0])
    elif pathname == '/table':
        # Таблица - Макет приложения с боковым меню sidebar и правой панелью
        # content1
        # print(html.Div([sidebar, content1]))
        return html.Div([sidebar, content1])
    elif pathname == '/graph':
        # Графики - Макет приложения с боковым меню sidebar и правой панелью
        # content2
        return html.Div([sidebar, content2])
    else:
        # Если пользователь попытается перейти на другую страницу,
        # вернём сообщение 404.
        return html.Div([
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"Путь {pathname} не распознан..."),
        ],
            className="p-3 bg-light rounded-3",
        )
    # endif
# enddef
# ============================================================================
if __name__ == "__main__":
    app.run_server(debug=True)
    # app.run_server(debug=True, use_reloader=False)
# endif
# ============================================================================

# Не работает
# Callback error updating page-content.children
