# https://dash.plotly.com/urls#multi-page-apps-without-pages

from dash import Dash, dcc, html, callback, Input, Output

app = Dash(__name__)

app.layout = html.Div([
    # Представляет адресную строку браузера и ничего не отображает
    dcc.Location(id='url', refresh=False),

    dcc.Link('Navigate to "/"', href='/'),
    html.Br(),
    dcc.Link('Navigate to "/page-2"', href='/page-2'),

    # Содержимое страницы будет отображаться в этом элементе
    html.Div(id='page-content')
])


@app.callback(
    Output('page-content', 'children'),
    Input('url', 'pathname')
)
def display_page(pathname):
    return html.Div([
        html.H3(f'Вы находитесь на странице {pathname}')
    ])


if __name__ == '__main__':
    app.run(debug=True)
