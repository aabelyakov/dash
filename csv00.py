import csv

# Сначала запишем файл 'names.csv', который затем прочитаем
# with open('names.csv', 'w', newline='') as csvfile:
#     fieldnames = ['first_name', 'last_name']
#     writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
#     writer.writeheader()
#     writer.writerow({'first_name': 'Baked', 'last_name': 'Beans'})
#     writer.writerow({'first_name': 'Lovely', 'last_name': 'Spam'})
#     writer.writerow({'first_name': 'Wonderful', 'last_name': 'Spam'})
#endwith

# Читаем CSV файл и смотрим, как работают атрибуты и метод.
with open("names.csv", "r") as csvfile:
    reader = csv.DictReader(csvfile)
    print('Диалект: ', reader.dialect)
    print('Следующая строка: ', reader.__next__())
    print('Прочитано строк: ', reader.line_num)
    print('Имена полей: ', reader.fieldnames)
    for row in reader:
        print(row['first_name'], row['last_name'])


# Диалект:  excel
# Следующая строка:  {'first_name': 'Baked', 'last_name': 'Beans'}
# Прочитано строк:  2
# Имена полей:  ['first_name', 'last_name']
