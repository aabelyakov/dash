from datetime import datetime
from dateutil.parser import parser

# date=datetime.strptime(row['diary.date'], '%Y-%m-%d').date(),
# time=datetime.strptime(row['diary.time'], '%H:%M:%S').time(),

some_date = datetime(2024, 1, 21)
now_date = datetime.now()

# Даты можно вычитать:
a = now_date - some_date
print(a)

# Результатом разницы some_date и now_date будет объект класса datetime.timedelta(),
# у которого есть метод days, им и воспользуйтесь:
print(type(a))
# <class 'datetime.timedelta'>

print(a.days)

today = datetime.now()
print(type(today), "today =", today)
print("iso =", today.isoformat())

b=str(today)
print(type(b), "a =", a)

print(today)
print("============== ФОРМАТ  ISO ============================")
print(today.isoformat())
print(today.isoformat(timespec='hours'))
print(today.isoformat(timespec='minutes'))
print(today.isoformat(timespec='seconds'))
print(today.isoformat(timespec='milliseconds'))
print(today.isoformat(timespec='microseconds'))


# datestr = today.isoformat()
# # datestr = str(today)
# print(datestr)
# date1 = parser.parse(datestr, today)
# print(date1)

print(datetime.now().isoformat('#'))
print(datetime.now().isoformat())

# print(b.isoformat())


date_time_str = '2018-06-29'
date_time_obj = datetime.strptime(date_time_str, '%Y-%m-%d')
print('Дата:', date_time_obj.date())
print('Время:', date_time_obj.time())
print('Дата и время:', date_time_obj)


