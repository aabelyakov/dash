from dash import Dash, dcc, html, Input, Output, State, callback

app = Dash(__name__)

app.layout = html.Div([
    html.Div(dcc.Input(id='input-on-submit', type='text')),
    html.Button('Submit', id='submit-val', n_clicks=0),
    html.Div(
        id='container-button-basic',
        children='Введи значение и нажми кнопку'
    )
])


@callback(
    Output('container-button-basic', 'children'),
    Input('submit-val', 'n_clicks'),
    State('input-on-submit', 'value'),
    prevent_initial_call=True
)
def update_output(n_clicks, value):
    return f'Введено значение "{value}", кнопка была нажата {n_clicks} раз'
#enddef

if __name__ == '__main__':
    app.run(debug=True)
#endif
