# Везде на этой странице, где вы видите fig.show(), вы можете отобразить одну и
# ту же фигуру в приложении Dash, передав ее в аргумент «figure» компонента
# Graph из встроенного пакета Dash_core_Components следующим образом:

import plotly.graph_objects as go # or plotly.express as px
fig = go.Figure() # or any Plotly Express function e.g. px.bar(...)
# fig.add_trace( ... )
# fig.update_layout( ... )

from dash import Dash, dcc, html

app = Dash()
app.layout = html.Div([
    dcc.Graph(figure=fig)
])

app.run_server(debug=True, use_reloader=False)  # Turn off reloader if inside Jupyter