# Часть приложения ДНЕВНИК ДИАБЕТИКА, в которой находится
# страница для визуализации графиков

from pydal import DAL, Field
import os
import csv
from datetime import datetime, date, time, timedelta
from dash import Dash, dcc, html, Input, Output, callback, dash_table
import dash_bootstrap_components as dbc
import pandas as pd

# ============================================================================
# НАСТРОЙКИ ПРОГРАММЫ
# ~~~~~~~~~~~~~~~~~~~
base = "diary.sqlite"
tbl = "diary"
CSV = "diary.csv"

# Расположение БД - Локально
PATH = ""
# Расположение БД - Отдалённо
# PATH = "/home/bel/Yandex.Disk/Python/Dash/Bin"

# Путь к файлу csv
pathcsv = os.path.join(PATH, CSV)
# print("pathcsv =", pathcsv)

# Подключение к базе данных
# db = DAL(URI, folder=PATH)

URI = f"sqlite://{base}"

db = DAL(
    # Строка соединения с основной БД
    URI,
    # ------------------------------------------------------------------------
    # Этот каталог должен существовать до момента создания БД
    folder=PATH
)
# ============================================================================
# Структура таблицы diary
db.define_table(
    'diary',
    Field('date', 'text'),
    Field('time', 'text'),
    Field('gluc', 'double'),
    Field('pres', 'text'),
    Field('short', 'double'),
    Field('long', 'double'),
    Field('med', 'text'),
    Field('act', 'text'),
    Field('dlt', 'boolean'),
)

db.commit()
# print(URI)
# print(db)
# print(db.tables)
# ============================================================================
# Заполняем таблицу данными
# if db(db.diary.id).count() == 0:
#     for i in range(1, 30):
#         db.diary.insert(
#             date="",      #datetime.now().strftime("%Y-%m-%d"),
#             time="",      #datetime.now().strftime("%H:%M"),
#             gluc=5.6,
#             pres="160/94-69",
#             short=0,
#             long=0,
#             med="",
#             act="",
#             dlt=False
#         )
#     # endfor
# endif

# Удаление таблицы diary и ее повторное создание
# db.diary.drop()

# Обрезка (truncate) таблицы diary, то есть, удаление из неё всех записей и
# сброс счетчика id в 1.
# db.diary.truncate('RESTART IDENTITY CASCADE')
# ============================================================================
# Запрос на вывод всех записей из таблицы diary
entries = db(db.diary).select()
# print(entries)

data = [{
    'diary.id': entry.id,
    'diary.date': entry.date,
    'diary.time': entry.time,
    'diary.gluc': entry.gluc,
    'diary.pres': entry.pres,
    'diary.short': entry.short,
    'diary.long': entry.long,
    'diary.med': entry.med,
    'diary.act': entry.act,
    'diary.dlt': entry.dlt
} for entry in entries
]
# ============================================================================
# Для панели меню используем фиксированную позицию и фиксированную ширину
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "13rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}
# ============================================================================
# Содержимое размещается справа от панели меню и приобретает отступы
CONTENT_STYLE = {
    "margin-left": "12rem",
    "margin-right": "0.5rem",
    "padding": "2rem 1rem"
}
# ============================================================================
#  Макет выборщика начальной и конечной дат временного интервала
picler = dcc.DatePickerRange(
    id='date-picker-range',
    # start_date=datetime.now() - timedelta(days=14),
    # start_date=datetime.now() - timedelta(weeks=8),
    start_date=datetime(2023, 1, 1, 0, 0, 0),
    end_date=datetime.now(),
    display_format='YYYY-MM-DD',
    persistence=True,
    persistence_type='session'
),
# ============================================================================
# ГРАФИК
data = pd.read_csv("avocado.csv")
data = data.query("type == 'conventional' and region == 'Albany'")
data["Date"] = pd.to_datetime(data["Date"], format="%Y-%m-%d")
data.sort_values("Date", inplace=True)

# Панель графиков
content2 = html.Div(children=[
    html.H1(children="Аналитика продаж авокадо"),
    html.P(
        children="Поведение цен на авокадо",
    ),
    dcc.Graph(
        figure={
            "data": [{
                "x": data["Date"],
                "y": data["AveragePrice"],
                "type": "lines",
            }, ],
            "layout": {"title": "Средняя цена авокадо"},
        },
    ),
],
    style=CONTENT_STYLE
)


# ============================================================================
def exp_csv():
    """
    Экспорт данных из таблицы diary в текстовый файл diary.csv
    """
    entries.export_to_csv_file(open(pathcsv, 'w'))

    print("Экспорт данных из таблицы diary в текстовый файл diary.csv")
# enddef
# ============================================================================
def imp_csv():
    """
    Импорт данных из текстового файла diary.csv в таблицу diary
    """
    # Обрезка (truncate) таблицы diary, то есть, удаление из неё всех
    # записей и сброс счетчика id в 1.
    # db.diary.truncate('RESTART IDENTITY CASCADE')

    db.diary.import_from_csv_file(open(pathcsv, 'r'))
    # db.diary.import_from_csv_file(open(pathcsv, 'r', newline=''))

    # with open(pathcsv, "r", newline='') as csvfile:
    #     reader = csv.DictReader(csvfile)
    #     for row in reader:
    #         db.diary.insert(
    #             date=row['diary.date'],
    #             time=row['diary.time'],
    #             gluc=float(row['diary.gluc']),
    #             pres=row['diary.pres'],
    #             short=float(row['diary.short']),
    #             long=float(row['diary.long']),
    #             med=row['diary.med'],
    #             act=row['diary.act'],
    #             dlt=bool(row['diary.dlt'])
    #         )
    # endfor
    # endwith
    db.commit()

    print("Импорт данных из текстового файла diary.csv в таблицу diary")
# enddef
# ============================================================================
# exp_csv()
# imp_csv()
# ============================================================================
