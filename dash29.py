from dash import Dash, Input, Output, callback, dash_table
import pandas as pd
import dash_bootstrap_components as dbc

# Создайте экземпляр Dash
app = Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])

# Создайте таблицу данных
df = pd.read_csv('https://git.io/Juf1t')
datatable = dash_table.DataTable(
    df.to_dict('records'),
    [{"name": i, "id": i} for i in df.columns],
    id='tbl'
)

# Создайте кнопку "Создать новую запись"
create_button = dbc.Button(
    "Новая запись", id="create-button", n_clicks=0
)


# Создайте контейнер для таблицы и кнопки
container = dbc.Container([
    dbc.Label('Click a cell in the table:'),
    datatable,
    dbc.Alert(id='tbl_out'),
    create_button
])

# Функция обратного вызова для обновления DataTable
@callback(
    Output('tbl', 'data'),
    Input('create-button', 'n_clicks')
)
def create_new_record(n_clicks):
    if n_clicks > 0:
        # Здесь вы можете добавить код для создания новой записи в базе данных
        new_record = {"Column 1": "Value 1", "Column 2": "Value 2"}
        # Текущее состояние datatable
        current_data = datatable.data
        # После создания новой записи, обновите данные в DataTable
        current_data.append(new_record)
        return current_data
    else:
        return datatable.data

# Запустите приложение Dash
if __name__ == "__main__":
    app.layout = container
    app.run(debug=True)


