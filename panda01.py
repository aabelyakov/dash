import pandas as pd

df = pd.DataFrame({
    'first_name': ['John', 'Jane', 'Marry', 'Victoria', 'Gabriel', 'Layla'],
    'last_name': ['Smith', 'Doe', 'Jackson', 'Smith', 'Brown', 'Martinez'],
    'age': [34, 29, 37, 52, 26, 32]},
    index=['id001', 'id002', 'id003', 'id004', 'id005', 'id006'])

print(df.to_string())
# id001       John     Smith   34
# id002       Jane       Doe   29
# id003      Marry   Jackson   37
# id004   Victoria     Smith   52
# id005    Gabriel     Brown   26
# id006      Layla  Martinez   32

for col_name, data in df.items():
    print("col_name:",col_name, "\ndata:",data)
# col_name: first_name
# data: id001        John

for i, row in df.iterrows():
    print(f"Index: {i}")
    print(f"{row}\n")


# ==============
df = pd.DataFrame({
    'age': 18,
    'name': ['Alice', 'Bob', 'Carl'],
    'cardio': [60, 70, 80]
})

print(df)
#    age   name  cardio
# 0   18  Alice      60
# 1   18    Bob      70
# 2   18   Carl      80

# Select by Column
print(df["name"])
# 0    Alice
# 1      Bob
# 2     Carl
# Name: name, dtype: object

print(df['age'])
# 0    18
# 1    18
# 2    18
# Name: age, dtype: int64

# Select by Index and Slice
print(df[2:3])
#    age  name  cardio
# 2   18  Carl      80

print(df[2:])
#    age  name  cardio
# 2   18  Carl      80

print(df.iloc[2, 1])
# Carl

# Boolean Indexing
print(df[df['cardio']>60])
#    age  name  cardio
# 1   18   Bob      70
# 2   18  Carl      80

print(df['cardio'] > 60)
# 0    False
# 1     True
# 2     True
# Name: cardio, dtype: bool

# Select by Label
print(df.loc[:, 'name'])
# 0    Alice
# 1      Bob
# 2     Carl
# Name: name, dtype: object

print(df.loc[:, ['age', 'cardio']])
#    age  cardio
# 0   18      60
# 1   18      70
# 2   18      80

# Modifying an Existing DataFrame
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Чтобы скопировать одно целое число во все строки столбца, pandas
# использует широковещание!!!!!!!!!
df['age'] = 16
print(df)
   # age   name  cardio
# 0   16  Alice      60
# 1   16    Bob      70
# 2   16   Carl      80

df = pd.DataFrame({
    'age': 18,
    'name': ['Alice', 'Bob', 'Carl'],
    'cardio': [60, 70, 80]
})
print(df)
#    age   name  cardio
# 0   18  Alice      60
# 1   18    Bob      70
# 2   18   Carl      80

df.loc[1:,'age'] = 16
print(df)
#    age   name  cardio
# 0   18  Alice      60
# 1   16    Bob      70
# 2   16   Carl      80

# Чтобы разнообразить наши примеры, мы будем использовать новую систему,
# поскольку pandas очень гибкая система. К понимая различные схемы индексации
# — скобочные обозначения, срезы, loc и iloc — вы сможете перезаписать
# существующие данные и добавить новые данные. Здесь мы добавляем новый
# столбец friend, функция loc с индексом, нарезка (слайс) и широковещание:

df.loc[:,'friend'] = 'Alice'
print(df)
#    age   name  cardio friend
# 0   18  Alice      60  Alice
# 1   16    Bob      70  Alice
# 2   16   Carl      80  Alice

# Обратите внимание, что того же самого можно добиться с помощью более
# простого кода:
df['friend'] = 'Alice'
print(df)
#    age   name  cardio friend
# 0   18  Alice      60  Alice
# 1   16    Bob      70  Alice
# 2   16   Carl      80  Alice
