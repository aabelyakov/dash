# app.py
from dash import Dash, callback
from dash.dependencies import Input, Output
from dash import html
from dash import dcc

app = Dash(__name__)

# Определение макета приложения
app.layout = html.Div([
    dcc.Input(id='input-id', type='text', value='Initial text'),
    html.Div(id='output-div'),
])

@callback(
    Output('output-div', 'children'),
    Input('input-id', 'value')
)
def out(value):
     return html.Div(f"{value}", id='output-div')
#enddef

if __name__ == "__main__":
    app.run_server(debug=True)
