from hired1 import db
from io import StringIO
import dash
from dash import html, dash_table, dcc, ctx
from dash.dependencies import Input, Output
import pandas as pd

# Текущая страница
page = 1


def create_mem(rows):
    """Создание CSV в памяти
    """
    output = StringIO()
    rows.export_to_csv_file(output)
    csv_mem = output.getvalue()
    return StringIO(csv_mem)


def create_dfs(i: int):
    """ Создание df (DataFrame) для каждой таблицы
    """
    rows = db(
        (db.sch.id == db.cls.sch_id) &
        (db.cls.id == db.std.cls_id) &
        (db.cls.id == page)
    ).select()

    if rows:
        row = rows[0]
    else:
        print("Ошибка. Пустой результат запроса!")

    SchId = row.sch.id
    ClsId = row.cls.id

    rows1 = db(db.sch.id == SchId).select()
    csv1 = create_mem(rows1)
    df1 = pd.read_csv(csv1)

    rows2 = db(db.cls.id == ClsId).select()
    csv2 = create_mem(rows2)
    df2 = pd.read_csv(csv2)

    rows3 = db(db.std.cls_id == page).select()
    csv3 = create_mem(rows3)
    df3 = pd.read_csv(csv3)

    return df1, df2, df3


# Создание df (DataFrame) для каждой таблицы
df1, df2, df3 = create_dfs(page)

# Макет приложения
app = dash.Dash(__name__)

app.layout = html.Div([
    html.H1("Табличный редактор"),
    html.H3("Школы"),

    dash_table.DataTable(
        id='datatable1',
        columns=[
            {'name': 'id', 'id': 'sch.id'},
            {'name': 'Наименование школы', 'id': 'sch.name'},
            {'name': 'Адрес школы', 'id': 'sch.location'}
        ],
        data=df1.to_dict('records'),
        editable=True,
    ),

    html.H3("Классы"),

    dash_table.DataTable(
        id='datatable2',
        columns=[
            {'name': 'id', 'id': 'cls.id'},
            {'name': 'sch_id', 'id': 'cls.sch_id'},
            {'name': 'Наименование класса', 'id': 'cls.name'}
        ],
        data=df2.to_dict('records'),
        editable=True,
    ),

    html.H3("Ученики"),

    html.Div([
        # dash_html_components.Table(
        html.Table(
            id='datatable3',
            columns=[
                {'name': 'id', 'id': 'std.id'},
                {'name': "cls_id", 'id': "std.cls_1d"},
                {'name': 'Имя ученика', 'id': 'std.name'}
            ],
            style_table={'height': '300px', 'overflowY': 'auto'},
            data=df3.to_dict('records'),
            # editable=True,
        ),
        dcc.Pagination(
            id='pagination',
            current_page=0,
            page_size=10,
            total_size=len(df3)
        ),
    ]),

    html.Br(),
    html.H5("Перемещение по страницам"),

    html.Button("Назад", id="back-button", n_clicks=0),
    html.Button("Вперед", id="forward-button", n_clicks=0),
    html.Div(id='output-div1'),

    html.P(),

    html.Button("Сохранить изменения", id="save-button", n_clicks=0),
    html.Div(id='output-div2')
])


def update_database(df, tbl):
    if tbl == "sch":
        for index, row in df.iterrows():
            # Обновление записей в таблице sch

            db(db.sch.id == row['sch.id']).update(
                name=row['sch.name'],
                location=row['sch.location']
            )

    elif tbl == "cls":
        for index, row in df.iterrows():
            # Обновление записей в таблице cls
            db(db.cls.id == row['cls.id']).update(
                sch_id=row["cls.sch_id"],
                name=row['cls.name']
            )

    elif tbl == "std":
        for index, row in df.iterrows():
            # print(11, row['std.id'])
            # print(22,row['std.cls_id'])
            # print(33,row['std.name'])

            # Обновление записей в таблице std
            db(db.std.id == row['std.id']).update(
                cls_id=row["std.cls_id"],
                name=row['std.name']
            )

    db.commit()


@app.callback(
    [Output("output-div1", "children"),
     Output("datatable1", "data"),
     Output("datatable2", "data"),
     Output("datatable3", "data")],
    [Input("forward-button", "n_clicks"),
     Input("back-button", "n_clicks")],
)
def change_page(n_clicks_forward, n_clicks_back):
    global page

    changed_id = ctx.triggered_id

    if changed_id == "forward-button" and page < 6:
        page += 1
    elif changed_id == "back-button" and page > 1:
        page -= 1

    df1, df2, df3 = create_dfs(page)
    # msg = f"Нажата кнопка {changed_id}. Текущая страница: {page}"
    msg = f"Текущая страница: {page}"

    return (
        html.Div(msg, id="output-div1"),
        df1.to_dict('records'),
        df2.to_dict('records'),
        df3.to_dict('records')
    )


@app.callback(
    Output("output-div2", "children"),
    [Input("save-button", "n_clicks"),
     Input("datatable1", "data"),
     Input("datatable2", "data"),
     Input("datatable3", "data")],
)
def save_changes(n_clicks, data1, data2, data3):
    if n_clicks:
        # Преобразование данных из DataTable в DataFrame
        df1 = pd.DataFrame(data1)
        df2 = pd.DataFrame(data2)
        df3 = pd.DataFrame(data3)

        # Сохранение изменений в таблицы базы данных
        update_database(df1, "sch")
        update_database(df2, "cls")
        update_database(df3, "std")

        return html.Div("Изменения успешно сохранены!", id="output-div2")


if __name__ == '__main__':
    app.run_server(debug=True)

