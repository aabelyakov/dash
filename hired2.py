from hired1 import db
import pandas as pd

def create_data(ClsId):
    # Мгновенный снимок соединённых таблиц при заданном идентификаторе класса
    rows = db(
        (db.sch.id == db.cls.sch_id) &
        (db.cls.id == db.std.cls_id) &
        (db.cls.id == ClsId)
    ).select()

    # print(rows)

    if rows:
        row = rows[0]
    else:
        print("Ошибка. Не могу объединить таблицы sch, cls и std!")
    #endif

    # Идентификатор школы
    SchId = row.sch.id

    # Одна запись из таблицы sch Школы
    rows1 = db(db.sch.id==SchId).select()
    # print(111, rows1)

    # Список data1 со словарем (строкой) из таблицы sch Школы
    data1 = [{
        'sch.id': row.id,
        'sch.name': row.name,
        'sch.location': row.location,
    } for row in rows1
    ]

    # Одна запись из таблицы cls Классы
    rows2 = db(db.cls.id==ClsId).select()
    # print(222, rows2)

    # Список data2 со словарём (строкой) из таблицы cls Классы
    data2 = [{
        'cls.id': row.id,
        'cls.sch_id': row.sch_id,
        'cls.name': row.name,
    } for row in rows2
    ]

    # Одна запись из таблицы std Ученики
    rows3 = db(db.std.cls_id == ClsId).select()
    # print(333, rows3)

    # Список data3 со словарём (строкой) из таблицы std Ученики
    data3 = [{
        'std.id': row.id,
        'std.cls_id': row.cls_id,
        'std.name': row.name,
    } for row in rows3
    ]

    return data1, data2, data3
#enddef
# ============================================================================
data1, data2, data3 = create_data(1)
print(data1)
print(data2)
print(data3)

MAX = db(db.cls).count()
print(MAX)
