
                            # Проба меню

# ============================================================================
from dash import (
    Dash, dcc, html, Input, Output, callback, dash_table, ctx, callback_context
)
import dash_bootstrap_components as dbc
# ============================================================================
app = Dash(
    __name__,
    suppress_callback_exceptions=True,
    external_stylesheets=[dbc.themes.BOOTSTRAP]
)
# ============================================================================
# Для панели меню используем фиксированную позицию и фиксированную ширину
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "13rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}
# ============================================================================
# Макет блока панели меню
sidebar = html.Div([
    html.H3("Дневник диабетика"),

    html.Hr(),

    html.P("Доступные действия:", ),

    dbc.Nav([
        dbc.NavLink("Домой", href="/", active="exact"),
        dbc.NavLink("Карточка", href="/card", active="exact"),
        dbc.NavLink("Таблица", href="/table", active="exact"),
        dbc.NavLink("График", href="/graph", active="exact"),
    ],
        vertical=True,
        pills=True
    ),
],
    # Таблица стилей для панели меню
    style=SIDEBAR_STYLE,
)
# ============================================================================
# Макет-прародитель приложения. В этот Div при переключении страниц будут
# помещаться:
# - sidebar - панель меню,
# - sidebar+card - панель меню и панель суточных карточек,
# - sidebar+table - панель меню и панель таблицы,
# - sidebar+graph - панель меню и панель графиков.
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])
# ============================================================================
@callback(
    Output('page-content', 'children'),
    Input('url', 'pathname'),
)
def display_page(pathname):
    # Определение, какой компонент вызвал колбек
    component = callback_context.triggered[0]['prop_id'].split('.')[0]
    print(component)
    return sidebar
#enddef
# ============================================================================
if __name__ == "__main__":
    app.run_server(debug=True)
# endif
# ============================================================================
