# callbacks.py
from dash.dependencies import Input, Output
from app2 import app

# Определение функции-колбека
@app.callback(
    Output('output-div', 'children'),
    Input('input-id', 'value')
)
def update_output(value):
    return f'Output: {value}'
#enddef
