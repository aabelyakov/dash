"""
This file defines actions, i.e. functions the URLs are mapped into
The @action(path) decorator exposed the function at URL:

    http://127.0.0.1:8000/{app_name}/{path}

If app_name == '_default' then simply

    http://127.0.0.1:8000/{path}

If path == 'index' it can be omitted:

    http://127.0.0.1:8000/

The path follows the bottlepy syntax.

@action.uses('generic.html')  indicates that the action uses the generic.html
                              template
@action.uses(session)         indicates that the action uses the session
@action.uses(db)              indicates that the action uses the db
@action.uses(T)               indicates that the action uses the i18n &
                              pluralization
@action.uses(auth.user)       indicates that the action requires a logged in
                              user
@action.uses(auth)            indicates that the action requires the auth
                              object

session, db, T, auth, and tempates are examples of Fixtures.
Warning: Fixtures MUST be declared with @action.uses({fixtures}) else your
app will result in undefined behavior
"""


import os
from py4web import action, request, abort, redirect, URL
from yatl.helpers import A
from .common import (
    db, session, T, cache, auth, logger, authenticated,
    unauthenticated, flash)


@action("index")
@action.uses("index.html", auth, T)
def index():
    user = auth.get_user()
    message = T("Hello {first_name}".format(**user) if user else "Приветик!")
    actions = {"allowed_actions": auth.param.allowed_actions}
    return dict(message=message, actions=actions)
# enddef

# @action("imp_all_csv")
# @action.uses("index.html", db)
# def imp_all_csv():
#     """
#     Импорт данных из файла all.csv во все таблицы
#     """
#
#     db.import_from_csv_file(
#         open('apps/aab/databases/Csv/all.csv')
#     )
#     message = "Импорт данных из файла all.csv успешно завершён"
#     return dict(message=message)
# enddef
#
# @action("exp_all_csv")
# @action.uses("index.html", db)
# def exp_all_csv():
#     """
#     Экспорт данных изо ВСЕХ таблиц в текстовый файл all.csv
#     """
#     for tbl in db.tables:
#         if not db[tbl].get("_extra"):
#             # У служебных и оперативных таблиц пользовательский атрибут
#             # _extra отсутствует
#
#             # Удаление всех записей из служебной или оперативной таблицы tbl
#             # и установка идентификатора записи в 1
#             db[tbl].truncate('RESTART IDENTITY CASCADE')
#         # endif
#     with open('apps/aab/databases/Csv/all.csv', "w") as csvfile:
#         db.export_to_csv_file(csvfile)
#     # endwith
#     message = "Экспорт данных изо ВСЕХ таблиц в текстовый файл all.csv " \
#               "успешно завершён"
#     return dict(message=message)
# # enddef


@action("exp_spr_csv")
@action.uses("index.html", db)
def exp_spr_csv():
    """
    Экспорт данных из СПРАВОЧНЫХ таблиц БД в текстовый файл spr.csv
    """
    with open('apps/aab/databases/Csv/spr.csv', "w") as csvfile:
        for tbl in db.tables:
            if db[tbl].get("_extra") == "spr":
                # Текущая таблица - справочная, так как у справочных таблиц
                # пользовательский атрибут _extra установлен в значение
                # "spr" (см. файл models.py)

                # Запись в файл слова TABLE и имени таблицы tbl
                csvfile.write(f"TABLE {tbl}\r\n")

                # Запись в файл заголовков и данных полей таблицы tbl
                csvfile.write(str(db(db[tbl]).select()))

                # Запись в файл двух пустых строк
                csvfile.write("\r\n\r\n")
            # endif
        # endfor

        # Запись в файл слова END
        csvfile.write("END")
    # endwith
    message = "Экспорт данных из справочных таблиц БД в текстовый файл " \
              "spr.csv успешно завершён"
    return dict(message=message)
# enddef


@action("imp_spr_csv")
@action.uses("index.html", db)
def imp_spr_csv():
    """
    Импорт данных из текстового файла spr.csv в СПРАВОЧНЫЕ таблицы БД
    """
    with open('apps/aab/databases/Csv/spr.csv') as csvfile:
        db.import_from_csv_file(
            csvfile,
            restore=True
        )
    # endwith
    message = "Импорт данных из текстового файла spr.csv в справочные " \
              "таблицы БД успешно завершён"
    return dict(message=message)
# enddef
