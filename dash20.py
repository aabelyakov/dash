# Работает!!!

import dash
from dash import html, dash_table
from dash.dependencies import Input, Output
from pydal import DAL, Field
import pandas as pd

# Создаем подключение к базе данных SQLite
db = DAL("sqlite://storage.sqlite")

# Определяем таблицу "Школы", если она еще не существует
db.define_table(
    'schools',
    Field('name', length=100, unique=True),
    Field('location', length=100)
)

# Определяем таблицу "Классы", если она еще не существует
db.define_table(
    'classes',
    Field('school_id', 'reference schools'),
    Field('name', length=100)
)

# Определяем таблицу "Ученики", если она еще не существует
db.define_table(
    'students',
    Field('class_id', 'reference classes'),
    Field('name', length=100)
)

# Проверяем, есть ли записи в таблице "Школы"
if db(db.schools.id > 0).count() == 0:
    # Вставляем данные в таблицу "Школы"
    db.schools.insert(name="Школа №1", location="Город A")
    db.schools.insert(name="Школа №2", location="Город B")

# Проверяем, есть ли записи в таблице "Классы"
if db(db.classes.id > 0).count() == 0:
    # Вставляем данные в таблицу "Классы"
    db.classes.insert(name="Класс A1", school_id=1)
    db.classes.insert(name="Класс A2", school_id=1)
    db.classes.insert(name="Класс B1", school_id=2)

# Проверяем, есть ли записи в таблице "Ученики"
if db(db.students.id > 0).count() == 0:
    # Вставляем данные в таблицу "Ученики"
    for class_id in range(1, 4):
        for student_number in range(1, 5):
            db.students.insert(name=f"Ученик{student_number}", class_id=class_id)

# Применяем изменения в базе данных
db.commit()

# Выполняем объединение всех трех таблиц в одном запросе
result_tree = db(
    (db.schools.id == db.classes.school_id) &
    (db.classes.id == db.students.class_id)).select()

# Собираем данные вручную для dash_table.DataTable
data = []
for row in result_tree:
    data.append({
        'School ID': row.schools.id,
        'School Name': row.schools.name,
        'School Location': row.schools.location,
        'Class ID': row.classes.id,
        'Class Name': row.classes.name,
        'Student ID': row.students.id,
        'Student Name': row.students.name,
        'Class ID (Student)': row.students.class_id,
    })

# Инициализируем приложение Dash
app = dash.Dash(__name__)

# Задаем layout
app.layout = html.Div([
    html.H1("Web-приложение для редактирования данных"),

    # Добавляем dash_table.DataTable
    dash_table.DataTable(
        id='table',
        columns=[
            {'name': col, 'id': col, 'editable': True} for col in data[0].keys()
        ],
        data=data,
        editable=True,
        row_deletable=True,
        style_table={'height': '300px', 'overflowY': 'auto'},
    ),
])

# Запускаем приложение Dash
if __name__ == "__main__":
    app.run_server(debug=True)

