from dash import Dash, html, Input, Output, ctx, callback

app = Dash(__name__)

app.layout = html.Div([
    html.Button('Кнопка-1', id='btn-nclicks-1', n_clicks=0),
    html.Button('Кнопка-2', id='btn-nclicks-2', n_clicks=0),
    html.Button('Кнопка-3', id='btn-nclicks-3', n_clicks=0),
    html.Div(id='container-button-timestamp')
])

@callback(
    Output('container-button-timestamp', 'children'),
    Input('btn-nclicks-1', 'n_clicks'),
    Input('btn-nclicks-2', 'n_clicks'),
    Input('btn-nclicks-3', 'n_clicks')
)
def displayClick(btn1, btn2, btn3):
    msg = "Не нажата ни одна кнопка"
    if "btn-nclicks-1" == ctx.triggered_id:
        msg = "Кнопка-1 была нажата"
    elif "btn-nclicks-2" == ctx.triggered_id:
        msg = "Кнопка-2 была нажата"
    elif "btn-nclicks-3" == ctx.triggered_id:
        msg = "Кнопка-3 была нажата"
    #endif
    return html.Div(msg)
#enddef

if __name__ == '__main__':
    app.run(debug=True)
#endif
