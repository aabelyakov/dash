#  File "/home/bel/PycharmProjects/dash/venv/lib/python3.11/site-packages/dash_mantine_components/AccordionItem.py",
#  line 54, in __init__
#     raise TypeError(
# TypeError: Required argument `value` was not specified.

from dash import Dash
import dash_mantine_components as dmc
from dash_iconify import DashIconify

app = Dash(__name__)

pages = ["home.py", "page1.py", "page2.py"]
assets = ["mycss.css", "app.png", "page1.png"]


def make_file(file_name):
    return dmc.Text([
        DashIconify(icon="akar-icons:file"),
        " ",
        file_name
    ],
        style={"paddingTop": 10}
    )


def make_folder(folder_name):
    return [DashIconify(icon="akar-icons:folder"), " ", folder_name]


file_dir = dmc.Accordion([
    dmc.AccordionItem(
        [make_file(f) for f in assets],
        label=make_folder("assets")
    ),

    dmc.AccordionItem(
        [make_file(f) for f in pages],
        label=make_folder("pages")
    ),
],
    multiple=True,
)

app.layout = dmc.Container([file_dir, make_file("app1.py")])

if __name__ == "__main__":
    app.run_server(debug=True)
