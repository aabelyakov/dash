# Дневник диабетика
# =================

# Приложение содержит следующие программные модули (см. diary.txt):
# - diary.py - главный модуль
# - card.py - модуль для работы с карточками
# - table.py - модуль для работы с таблицей
# - graph.py - модуль для просмотра графиков
# В папке assets хранятся таблицы каскадных стилей, применяемых в приложении
# ============================================================================
# Импорт дополнительных модулей приложения
from card import *
from table import *
from graph import *
# ============================================================================
from pydal import DAL, Field
import os
import csv
# from datetime import datetime, date, time, timedelta
from dash import (
    Dash, dcc, html, Input, Output, callback,
    dash_table, ctx, callback_context
)
import dash_bootstrap_components as dbc
import pandas as pd
from dash.dependencies import Input, Output, State
# ============================================================================
# НАСТРОЙКИ ПРИЛОЖЕНИЯ
# ~~~~~~~~~~~~~~~~~~~~
base = "diary.sqlite"
tbl = "diary"
CSV = "diary.csv"

# Расположение БД - Локально
PATH = ""
# Расположение БД - Отдалённо
# PATH = "/home/bel/Yandex.Disk/Python/Dash/Bin"

# Путь к файлу csv
pathcsv = os.path.join(PATH, CSV)
# print("pathcsv =", pathcsv)

# Подключение к базе данных
# db = DAL(URI, folder=PATH)

URI = f"sqlite://{base}"

db = DAL(
    # Строка соединения с основной БД
    URI,
    # ------------------------------------------------------------------------
    # Этот каталог должен существовать до момента создания БД
    folder=PATH
)
# ============================================================================
# Структура таблицы diary
db.define_table(
    'diary',
    Field('date', 'text'),
    Field('time', 'text'),
    Field('gluc', 'double'),
    Field('pres', 'text'),
    Field('short', 'double'),
    Field('long', 'double'),
    Field('med', 'text'),
    Field('act', 'text'),
    Field('dlt', 'boolean'),
)

db.commit()

# print(URI)
# print(db)
# print(db.tables)
# ============================================================================
# Вывод всех записей из таблицы diary
entries = db(db.diary).select()
# print(entries)

data = [{
    'diary.id': entry.id,
    'diary.date': entry.date,
    'diary.time': entry.time,
    'diary.gluc': entry.gluc,
    'diary.pres': entry.pres,
    'diary.short': entry.short,
    'diary.long': entry.long,
    'diary.med': entry.med,
    'diary.act': entry.act,
    'diary.dlt': entry.dlt
} for entry in entries
]
# ============================================================================
# dbc_css = "https://cdn.jsdelivr.net/gh/AnnMarieW/dash-bootstrap-templates/dbc.min.css"

# app = Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
# app = Dash(__name__, suppress_callback_exceptions=True)
# app = Dash(
#     __name__,
#     external_stylesheets=[dbc.themes.BOOTSTRAP],
#     prevent_initial_callbacks=True
# )

app = Dash(
    __name__,
    suppress_callback_exceptions=True,
    external_stylesheets=[dbc.themes.BOOTSTRAP]
)
# ============================================================================
# Макет-прародитель приложения. В этот Div при переключении страниц будут
# помещаться:
# - sidebar - панель меню,
# - sidebar+card - панель меню и панель суточных карточек,
# - sidebar+table - панель меню и панель таблицы,
# - sidebar+graph - панель меню и панель графиков.
app.layout = html.Div([
    dcc.Location(id='url'),
    # dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])
# ============================================================================
# Для панели меню используем фиксированную позицию и фиксированную ширину
sdb1 = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "13rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}
# ============================================================================
# Содержимое размещается справа от панели меню и приобретает отступы
content1 = {
    "margin-left": "12rem",
    "margin-right": "0.5rem",
    "padding": "2rem 1rem"
}
# ============================================================================
# Макет блока панели меню
sidebar = html.Div([
    html.H3("Дневник диабетика"),

    html.Hr(),

    html.P("Доступные действия:", ),

    dbc.Nav([
        dbc.NavLink("Домой", href="/", active="exact"),
        dbc.NavLink("Карточка", href="/card", active="exact"),
        dbc.NavLink("Таблица", href="/table", active="exact"),
        dbc.NavLink("График", href="/graph", active="exact"),
    ],
        vertical=True,
        pills=True
    ),
],
    # Таблица стилей для панели меню
    className="sdb",
    # class_name="sdb",
    # style=sdb
)
# ============================================================================
# Макет блока таблицы
tbl = dash_table.DataTable(
    id='datatable',
    columns=[
        {'name': 'id', 'id': 'diary.id', 'editable': False},
        {'name': 'Дата', 'id': 'diary.date'},
        {'name': 'Время', 'id': 'diary.time'},
        {'name': 'Сахар', 'id': 'diary.gluc'},
        {'name': 'Давление', 'id': 'diary.pres'},
        {'name': 'Короткий', 'id': 'diary.short'},
        {'name': 'Длинный', 'id': 'diary.long'},
        {'name': 'Лекарства', 'id': 'diary.med'},
        {'name': 'Активность', 'id': 'diary.act'},
        {'name': 'К удалению', 'id': 'diary.dlt'},
    ],

    # data=[],
    data=data,

    editable=True,

    # Самый левый столбец в таблице с крестикакми х для удаления записей ПРОВЕРИТЬ!!!!
    # row_deletable=True,

    # Высота таблицы и скроллинг
    style_table={'height': '550px', 'overflowY': 'auto'},

    # Перемещение по страницам таблицы высотой в 15 строк
    # page_size=15,
)
# ============================================================================
# Макет блока таблицы с кнопками и сообщениями
table = html.Div([
    # Таблица с данными
    tbl,

    html.Br(),

    html.Button("Сохранить изменения", id="save-button", n_clicks=0),

    # Для вывода сообщения об успешном завершении сохранения
    html.Div(id='output-save'),

    html.Br(),

    html.Button("Новая", id="new-button", n_clicks=0),
    html.Button("Удалить", id="del-button", n_clicks=0),

    # Для вывода сообщения об успешном создании новой записи и удалении
    # текущей
    html.Div(id='output-new'),

    # Для вывода сообщения об удалении помеченных записей
    html.Div(id='del-div'),

],
    # Таблица стилей для таблицы и кнопок
    # style=content
    className="content"
    # class_name="content"
)
# ============================================================================
# Макет блока панели графиков
graph = html.Div(children=[
    html.H1(children="Аналитика продаж авокадо"),

    html.P(children="Изменение цен на авокадо"),

    dcc.Graph(
        figure={
            "data1": [{
                "x": data1["Date"],
                "y": data1["AveragePrice"],
                "type": "lines",
            }, ],
            "layout": {"title": "Средняя цена авокадо"},
        },
    ),
],
    # style=content
    className="content"
    # class_name="content"
)
# ============================================================================
# Меню для переключения страниц приложения
# Выход - страница в браузере
# Вход - путь в поисковой строке браузера
@callback(
    Output('page-content', 'children', allow_duplicate=True),
    Input('url', 'pathname'),
    prevent_initial_call = True
)
def display_page(pathname):
    # Определение, какой компонент вызвал колбек
    # component = callback_context.triggered[0]['prop_id'].split('.')[0]
    # print(component)
    if pathname == '/':
        # Домой - Макет приложения с боковым меню sidebar
        return html.Div([sidebar])
    elif pathname == '/card':
        # Карточки - Макет приложения с боковым меню sidebar и правой панелью
        # card
        return html.Div([sidebar, card])
    elif pathname == '/table':
        # Таблица - Макет приложения с боковым меню sidebar и правой панелью
        # table
        return html.Div([sidebar, table])
    elif pathname == '/graph':
        # Графики - Макет приложения с боковым меню sidebar и правой панелью
        # graph
        return html.Div([sidebar, graph])
    else:
        # Если пользователь попытается перейти на другую страницу,
        # вернём сообщение 404.
        return html.Div([
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"Путь {pathname} не распознан..."),
        ],
            className="p-3 bg-light rounded-3",
        )
    #endif
# enddef
# ============================================================================
def exp_csv():
    """
    Экспорт данных из таблицы diary в текстовый файл diary.csv
    """
    entries.export_to_csv_file(open(pathcsv, 'w'))

    print("Экспорт данных из таблицы diary в текстовый файл diary.csv")
# enddef
# ============================================================================
def imp_csv():
    """
    Импорт данных из текстового файла diary.csv в таблицу diary
    """
    # Обрезка таблицы diary, то есть, удаление из неё всех
    # записей и сброс счетчика id в 1.
    # db.diary.truncate('RESTART IDENTITY CASCADE')
    # Эта команда аналог параметра   !!!!   restore = True    !!!!!

    db.diary.import_from_csv_file(open(pathcsv, 'r'), restore=True)
    db.commit()

    print("Импорт данных из текстового файла diary.csv в таблицу diary")
# enddef
# ============================================================================
# Запуск программы экспорта
# exp_csv()
# Запуск программы импорта
# imp_csv()
# ============================================================================
# Выход - создание в таблице diary новой записи
# Вход - нажатие кнопки "Новая"
@callback(
    [Output("datatable", "data", allow_duplicate=True),
    Output("output-new", "children", allow_duplicate=True)],
    Input("new-button", "n_clicks"),
    State("datatable", "data"),
    prevent_initial_call=True
)
def create_new(n_clicks, data):
    if n_clicks:
        # Вставка новой записи с new_id в таблицу diary БД
        new_id = db.diary.insert(
            date="",
            time="",
            gluc=0,
            pres="",
            short=0,
            long=0,
            med="",
            act="",
            dlt=False
        )

        # Сброс буферов программы на диск
        db.commit()

        # Запрос к таблице diary на выборку всех записей
        entries = db(db.diary).select()

        # print(entries)

        data = [{
            'diary.id': entry.id,
            'diary.date': entry.date,
            'diary.time': entry.time,
            'diary.gluc': entry.gluc,
            'diary.pres': entry.pres,
            'diary.short': entry.short,
            'diary.long': entry.long,
            'diary.med': entry.med,
            'diary.act': entry.act,
            'diary.dlt': entry.dlt
        } for entry in entries
        ]

        # print(data)
        # print(f"New ID: {new_id}")
        return [
            data,
            html.Div(f"Новая запись c Id={new_id} успешно создана!", id="output-new")
        ]

    # endif
# enddef
# ============================================================================
# Выход - вывод сообщения о сохранении изменений в поле output-save
# Вход - нажатие кнопки "Сохранить изменения"
# Вход - выполнение редактирования полей таблицы diary. Преобразование
# данных из DataFrame в DataTable для отображения
@callback(
    Output("output-save", "children"),
    [Input("save-button", "n_clicks"),
    Input("datatable", "data")]

    # Output("output-save", "children", allow_duplicate=True),
    # [Input("save-button", "n_clicks"),
    # Input("datatable", "data")],
    # prevent_initial_call = True
)
def save_changes(n_clicks, data):
    if n_clicks:
        # Преобразование данных из DataTable в DataFrame
        df = pd.DataFrame(data)
        # print(df)
        # Сохранение изменений в базе данных
        for index, row in df.iterrows():
            db(db.diary.id == row['diary.id']).update(
                date=row['diary.date'],
                time=row['diary.time'],
                gluc=round(float(row['diary.gluc']), 1),
                pres=row['diary.pres'],
                short=row['diary.short'],
                long=row['diary.long'],
                med=row['diary.med'],
                act=row['diary.act'],
                dlt=row['diary.dlt']
            )
        #endfor

        db.commit()

        return html.Div("Изменения успешно сохранены!", id="output-div")
    #endif
#enddef
# ============================================================================
# Вход - нажатие кнопки "Удалить"
# Выход - удаление помеченных записей из таблицы diary
# Выход - вывод сообщения об успешном удалении помеченных записей

@callback(
    [Output("datatable", "data", allow_duplicate=True),
    Output("output-new", "children", allow_duplicate=True)],
    Input("del-button", "n_clicks"),
    State("datatable", "data"),
    prevent_initial_call=True,
)
def del_records(n_clicks, data):
    if n_clicks:
        # Каким компонентом был вызван колбек?
        # component = callback_context.triggered[0]['prop_id'].split('.')[0]
        # print(component)

        # Удаление n помеченных записей из таблицы diary
        n = db(db.diary.dlt==True).delete()
        # print(n)
        db.commit()

        # Запрос к таблице diary на выборку всех записей
        entries = db(db.diary).select()

        data = [{
            'diary.id': entry.id,
            'diary.date': entry.date,
            'diary.time': entry.time,
            'diary.gluc': entry.gluc,
            'diary.pres': entry.pres,
            'diary.short': entry.short,
            'diary.long': entry.long,
            'diary.med': entry.med,
            'diary.act': entry.act,
            'diary.dlt': entry.dlt

        } for entry in entries
        ]

        return [
            data,
            html.Div(f"Успешно удалено записей: {n}", id="output-new")
        ]
    #endif
#enddef
# ============================================================================
if __name__ == "__main__":
    app.run_server(debug=True)
    # app.run_server(debug=True, use_reloader=False)
# endif
# ============================================================================

