# https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_dict.html

import pandas as pd

df = pd.DataFrame({
    'col1': [1, 2],
    'col2': [0.5, 0.75]},
    index=['row1', 'row2'])
    # index=['st1', 'st2'])

print(1, df)
#       col1  col2
# row1     1  0.50
# row2     2  0.75

print(10, df.info())
# None

print(11, df.dtypes)
# col1      int64
# col2    float64
# dtype: object

print(2, df.to_dict())
# {'col1': {'row1': 1, 'row2': 2}, 'col2': {'row1': 0.5, 'row2': 0.75}}
# =====================================================================

print(3, df.to_dict('series'))
# {'col1': row1    1
# row2    2
# Name: col1, dtype: int64, 'col2': row1    0.50
# row2    0.75
# Name: col2, dtype: float64}
# =====================================================================

print(4, df.to_dict('split'))
# {'index': ['row1', 'row2'], 'columns': ['col1', 'col2'], 'data': [[1, 0.5], [2, 0.75]]}
# =====================================================================

print(5, df.to_dict('records'))
# [{'col1': 1, 'col2': 0.5}, {'col1': 2, 'col2': 0.75}]
# =====================================================================

print(6, df.to_dict('index'))
# {'row1': {'col1': 1, 'col2': 0.5}, 'row2': {'col1': 2, 'col2': 0.75}}
# =====================================================================

print(7, df.to_dict('tight'))
# {'index': ['row1', 'row2'], 'columns': ['col1', 'col2'],
#  'data': [[1, 0.5], [2, 0.75]], 'index_names': [None], 'column_names': [None]}

d = {"b": 1, "a": 0, "c": 2}

x = pd.Series(d)
print(x)
# b    1
# a    0
# c    2
# dtype: int64