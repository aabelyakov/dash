# Боковая панель с навигационными ссылками

import dash
import dash_bootstrap_components as dbc
from dash import Input, Output, dcc, html

app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])

# Аргументы стиля для боковой панели. Используем фиксированную позицию и фиксированную ширину.
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "13rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}

# Стили основного контента размещают его справа от боковой панели и
# добавляют отступы
CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

sidebar = html.Div([
    html.H4("Дневник диабетика"),
    html.Hr(),
    html.P(
        "Доступные действия:",
        # className="lead"
    ),

    dbc.Nav([
        dbc.NavLink("Домой", href="/", active="exact"),
        dbc.NavLink("Таблица", href="/page-1", active="exact"),
        dbc.NavLink("Графики", href="/page-2", active="exact"),
    ],
        vertical=True,
        pills=True,
    ),
],
    style=SIDEBAR_STYLE,
)


content = html.Div(id="page-content", style=CONTENT_STYLE)

app.layout = html.Div([dcc.Location(id="url"), sidebar, content])


@app.callback(
    Output("page-content", "children"),
    [Input("url", "pathname")]
)
def render_page_content(pathname):
    if pathname == "/":
        return html.P("Это содержимое страницы Home")
    elif pathname == "/page-1":
        return html.P("Это содержимое страницы Page 1")
    elif pathname == "/page-2":
        return html.P("Это содержимое страницы Page 2")
    else:
        # Если пользователь попытается перейти на другую страницу, вернём сообщение 404.
        return html.Div([
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            # html.P(f"The pathname {pathname} was not recognised..."),
            html.P(f"Путь {pathname} не распознан..."),
        ],
            className="p-3 bg-light rounded-3",
        )
    #endif
#enddef

if __name__ == "__main__":
    app.run_server(debug=True)
