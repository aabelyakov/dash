from datetime import datetime

# Получение текущей даты и времени
date = datetime.now()

# Извлечение даты и времени
date_part = date.strftime("%Y.%m.%d") # Только дата
time_part = date.strftime("%H:%M") # Только время

# Вывод результатов
print("Дата:", date_part)
print("Время:", time_part)
