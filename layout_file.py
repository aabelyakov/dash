# file_layout.py
from dash import html

# Функция, определяющая макет
def get_layout():
    return html.Div([
        html.Button("Нажми меня", id="my-button"),
    ])
