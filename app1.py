# Пример наипростейшего многостраничного приложения
# Смотри файл layouts, из которого импортируются макеты страниц layout1
# и layout2

from dash import Dash, dcc, html, Input, Output, callback
from layouts import layout1, layout2
import dash_bootstrap_components as dbc

dbc_css = "https://cdn.jsdelivr.net/gh/AnnMarieW/dash-bootstrap-templates/dbc.min.css"
app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP, dbc_css])
# app = Dash(__name__, suppress_callback_exceptions=True)

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    # html.P("bg-primary", className="bg-primary"),
    html.Div(id='page-content'),
    dcc.Link('Go to Page 1', href='/page1'),
    dcc.Link('Go to Page 2', href='/page2')
])


@callback(
    Output('page-content', 'children'),
    Input('url', 'pathname')
)
def display_page(pathname):
    if pathname == '/':
        return []
    elif pathname == '/page1':
        return layout1
    elif pathname == '/page2':
        return layout2
    else:
        return '404'
    #endif
#enddef

if __name__ == '__main__':
    app.run(debug=True)
#endif
